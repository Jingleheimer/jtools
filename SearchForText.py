#!/usr/bin/env python3
#######################################################################################################################
##  Helper script for searching a file or set of files for text
#######################################################################################################################
import os
import sys
import subprocess
import argparse


#######################################################################################################################
##  Constants
#######################################################################################################################
EXTENSION_FILTERS = [".h", ".hpp", ".cpp", ".c", ".cs", ".idl", ".vhd", ".xml"]
PRUNE_FILTERS = [".svn", ".git"]

VERBOSE_INFO_LEVEL = 1
VERBOSE_DEBUG_LEVEL = 2


#######################################################################################################################
##  Setup and parse command-line arguments
#######################################################################################################################
parser = argparse.ArgumentParser(description='Search tool for finding text in specific file types',
                                 formatter_class = lambda prog: argparse.HelpFormatter(prog, max_help_position=400, width=300))
parser.add_argument('-a', '--all',action='store_true', help='Search all file extensions')
parser.add_argument('-e', '--extensions', nargs='*', metavar='EXTENSION', help=f"Add additional extension filter(s) to the default list: {' '.join(EXTENSION_FILTERS)}")
parser.add_argument('-o', '--only', nargs='*', metavar='EXTENSION', help='Only search files with the specified extension(s)')
parser.add_argument('-n', '--name', nargs='*', metavar='TEXT', help='Search only files with this text in their name')
parser.add_argument('-p', '--prune', nargs='*', metavar='TEXT', help=f"Add additional prune filter(s) to the default list: {' '.join(PRUNE_FILTERS)}")
parser.add_argument('-c', '--case', action='store_true', help='Enable case sensativity')
parser.add_argument('-r', '--replace', metavar='TEXT', dest='replace', help='Replace Term(s) with specified text')
parser.add_argument('-v', '--verbose', action='count', default=0, help='Print additional information. Info: -v, Debug: -vv')
parser.add_argument('path', metavar='PATH', help='File or directory to search in')
parser.add_argument('terms', nargs='*', metavar='TERM', help='Search term(s) to search for')
args = parser.parse_args()


########################################################################################################################
##  Debug print functions.
########################################################################################################################
def PrintInfo(message):
    if(args.verbose >= VERBOSE_INFO_LEVEL):
        print("INFO:\t" + str(message))

def PrintDebug(message):
    if(args.verbose >= VERBOSE_DEBUG_LEVEL):
        print("DEBUG:\t" + str(message))

def PrintWarning(message):
    print("WARN:\t" + str(message))

def PrintError(message):
    print("ERROR:\t" + str(message))
    sys.exit(1)

def WaitForInput(message, question):
    print(message)
    response = input(question + " ")
    print(response)
    return response


########################################################################################################################
##  Get list of target extensions
########################################################################################################################
def GetExtensionFilters(exclusiveExtensions, additionalExtensions):
    if(exclusiveExtensions != None):
        extensionFilters = exclusiveExtensions
    elif(additionalExtensions != None):
        extensionFilters = EXTENSION_FILTERS + additionalExtensions
    else:
        extensionFilters = EXTENSION_FILTERS

    PrintDebug(f"Extension filters: {str(extensionFilters)}")
    return extensionFilters


########################################################################################################################
##  Get list of text to prune
########################################################################################################################
def GetPruneFilters(pruneList):
    if(pruneList == None):
        pruneFilters = PRUNE_FILTERS
    else:
        pruneFilters = PRUNE_FILTERS + pruneList

    PrintDebug(f"Prune filters: {str(pruneFilters)}")
    return pruneFilters


########################################################################################################################
##  Build up arguments list for asking Find for a list of files
########################################################################################################################
def BuildFindArguments(searchAll, nameFilters, extensionFilters, pruneFilters):

    ## Base arguments
    findArgs = ['-type', 'f']

    ## Build extension filter arguments if not requested to search all extensions
    if(searchAll != True):
        findArgs.append('(')                    # Filtering names / paths with Find need to be closed in (), otherwise -exec only runs on the last logical operation
        for i, extension in enumerate(extensionFilters):
            if(i > 0):
                findArgs.append('-o')            # Only append the OR argument if there is more than one argument (e.g. after the first run of the loop)
            findArgs.append('-iname')            # Treat extensions as case insensative
            findArgs.append('*' + extension)     # We assume we are filtering for extensions to put the '*' out front
        findArgs.append(')')

    ## Build name filter arguments
    if(nameFilters != None):
        findArgs.append('(')
        for i, nameFilter in enumerate(nameFilters):
            findArgs.append('-iname')
            findArgs.append('*' + nameFilter + '*')
        findArgs.append(')')

    ## Build prune filter arguments
    findArgs.append('(')
    for i, ignoreFilter in enumerate(pruneFilters):
        findArgs.append('-not')              # Treat extensions as case insensative
        findArgs.append('-ipath')
        findArgs.append('*' + ignoreFilter + '*')
    findArgs.append(')')

    PrintDebug("Find Arguments: " + str(findArgs))
    return findArgs


########################################################################################################################
##  Build up Find Exec command(s)
########################################################################################################################
def BuildFindExecCommand(searchTerms, replaceText, grepArguments):

    ## Base arguments
    execCommand = ['-exec', 'grep'] + grepArguments

    ## SED exec to perform replacements
    if(replaceText != None):
        response = WaitForInput("!!! Text replacement activated. Enter Y to continue or CTL-C to abort. !!!", "Continue?")
        if(response == 'Y'):
            execCommand += ['-exec', 'sed', '-i.replaced']
            for i, term in enumerate(searchTerms):
                execCommand += ['-e', f"s/{term}/{replaceText}/g"]
            execCommand += ['{}', ';']
            #execCommand += ['-exec', 'grep'] + grepArguments + ['-E', replaceText, '{}', ';']   # Grep again for the changed words
        else:
            PrintWarning("Replace text not confirmed, searching only.")

    PrintDebug("Find Exec Command(s): " + str(execCommand))
    return execCommand


########################################################################################################################
##  Build up the Grep options and combine them into one string
##  Grep options
##      -n                Shows line numbers
##      -H                Forces filenames to be shown
##      -i                Ignore case
##      -s                No message. Avoides "is a directory" errors
##      -I                Ignore "binary" files (may include ignoring files with binary data. i.e. PDFs etc.)
##      --color=always    Shows color highlighting on results
########################################################################################################################
def BuildGrepArguments(caseSensative, searchTerms):

    ## Base arguments
    grepArguments = ['-H', '-s', '-I']

    ## If we are outputing to a file, dont add colore codes or line numbers
    if(sys.stdout.isatty() == False):
        grepArguments.append('--color=never')
    else:
        grepArguments.append('--color=always')
        grepArguments.append('-n')

    ## If we have not requested to be case sensative, let grep know it should be insensative
    if(caseSensative != True):
        grepArguments.append('-i')

    searchTermsString = ""
    for i, term in enumerate(searchTerms):
        if(i > 0):
            searchTermsString += '|'                # Only append the OR argument if there is more than one argument (e.g. after the first run of the loop)
        searchTermsString += term
    grepArguments += ['-E', searchTermsString, '{}', ';']

    PrintDebug("Grep Arguments: " + str(grepArguments))
    return grepArguments


########################################################################################################################
##  Main
########################################################################################################################
def Main(argv):

    ## Switch between the two major modes, either using Find to filter then grep files in a directory or grep a single file
    grepArguments = BuildGrepArguments(args.case, args.terms)
    if(os.path.isdir(args.path)):
        command = ['find', args.path]
        extensionFilters = GetExtensionFilters(args.only, args.extensions)
        pruneFilters = GetPruneFilters(args.prune)
        command += BuildFindArguments(args.all, args.name, extensionFilters, pruneFilters)

        # Check if Find discovers any files with the current config and print what it finds (if verbose) or an error so we have positive feedback.
        result = subprocess.run(command, stdout=subprocess.PIPE)
        foundFiles = result.stdout.decode('UTF-8').splitlines()
        if(len(foundFiles) > 0):
            PrintInfo("Searching in files:")
            for file in foundFiles:
                PrintInfo("\t" + file)
        else:
            PrintError("No files found with current filters")

        command += BuildFindExecCommand(args.terms, args.replace, grepArguments)
    else:
        command = ['grep'] + grepArguments + [args.path]

    ## Execute the command
    PrintDebug("Command: " + str(command))
    #PrintDebug(" ".join(command))      # Print in string form to copy and paste
    result = subprocess.run(command, stdout=subprocess.PIPE)
    foundLines = result.stdout.decode('UTF-8').splitlines()
    if(len(foundLines) > 0):
        for line in foundLines:
            print(line)
    else:
        PrintError("No text matching search terms")

if __name__ == "__main__":
   Main(sys.argv[1:])